import React from 'react';
import {Col, Row} from "antd";
import "../../style/basket/card-footer.scss";
import {Link} from "react-router-dom";
function CardFooter(props) {
    return (
        <div className="card-footer">
            <div className="container">
                <Row>
                    <Col span={16}>
                        <div className="card-service">
                            <Row>
                                <Col span={12}>
                                    <div className="card-service-content">
                                        <img
                                            src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/serviceicon1.png?v=1634645393"
                                            alt="image"/>
                                        <div className="text">
                                            <div className="title">Free Delivery</div>
                                            <div className="msg">Lorem Ipsum dummy</div>
                                        </div>
                                    </div>
                                </Col>
                                <Col span={12}>
                                    <div className="card-service-content">
                                        <img
                                            src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/serviceicon2.png?v=1634645403"
                                            alt="image"/>
                                        <div className="text">
                                            <div className="title">Big Savings</div>
                                            <div className="msg">Lorem Ipsum dummy</div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <div className="card-service-content">
                                        <img
                                            src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/serviceicon3.png?v=1634645412"
                                            alt="image"/>
                                        <div className="text">
                                            <div className="title">Customer Care</div>
                                            <div className="msg">Lorem Ipsum dummy</div>
                                        </div>
                                    </div>
                                </Col>
                                <Col span={12}>
                                    <div className="card-service-content">
                                        <img
                                            src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/serviceicon4.png?v=1634645422"
                                            alt="image"/>
                                        <div className="text">
                                            <div className="title">Gift Voucher</div>
                                            <div className="msg">Lorem Ipsum dummy</div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    <Col span={8}>
                        <div className="check-content">
                            <label htmlFor="text">Special Instructions</label>
                            <textarea id="text"/>
                            <button className="btn">Checkout</button>
                            <div className="link">
                                <Link to="/">Continue Shopping</Link>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default CardFooter;