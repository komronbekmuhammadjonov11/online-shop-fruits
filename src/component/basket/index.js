import React from 'react';
import Navbar from "../home/navbar";
import ShoppingCard from "./shopping-card";
import CardFooter from "./card-footer";
import Footer from "../home/footer";

function Basket(props) {
    return (
        <div className="basket">
           <Navbar/>
           <ShoppingCard/>
           <CardFooter/>
           <Footer/>
        </div>
    );
}

export default Basket;