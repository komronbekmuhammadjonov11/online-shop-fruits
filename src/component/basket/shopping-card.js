import React, {useContext} from 'react';
import {Col, Row} from "antd";
import {DataContext} from "../../App";
import "../../style/basket/shopping-card.scss";
function ShoppingCard(props) {
    const {card, setCard}=useContext(DataContext);
    console.log(card);
    const deleteCard=(id)=>{
        let newArray=[];
        newArray=card.filter(item=>item.id!==id);
        localStorage.setItem("card", JSON.stringify(newArray));
        setCard(newArray);
    };
    const decrement = (id) => {
        let newObject;
        let newArray = [];
        newObject = card.find(item => item.id === id);
        if (newObject.amount > 1) {
            newObject.amount -= 1;
            newArray = card.filter((item) => {
                if (item.id === id) {
                    return newObject;
                } else {
                    return item;
                }
            });
            setCard(newArray);
        }
    };
    const increment=(id)=>{
        let newObject;
        let newArray = [];
        newObject = card.find(item => item.id === id);
        newObject.amount += 1;
        newArray = card.filter((item) => {
            if (item.id === id) {
                return newObject;
            } else {
                return item;
            }
        });
        setCard(newArray);
    };
    const totalPrice=()=>{
        let totalPrice=0;
        card.forEach(item=>{
            totalPrice+=item.price*item.amount;
        });
      return totalPrice
    };
    return (
        <div className="shopping-card">
            <div className="container">
                <Row>
                    <Col span={24}>
                        <div className="shopping-card-title">
                            <h2>Your Shopping Cart</h2>
                        </div>
                        <table className="shopping-table">
                            <thead>
                            <tr>
                                <th colSpan={2}>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            {card.map(item => {
                                return <tr>
                                    <td colSpan={2}>
                                        <div className="images-content">
                                            <img
                                                src={item.url}
                                                alt={item.name}
                                            />
                                        </div>
                                    </td>
                                    <td>${item.price}</td>
                                    <td>
                                        <div className="button-content">
                                            <div className="button-group">
                                                <button className="decrement" onClick={()=>decrement(item.id)}>-</button>
                                                <button className="count">{item.amount}</button>
                                                <button className="increment" onClick={()=>increment(item.id)}>+</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td>${item.price*item.amount}</td>
                                    <div className="delete-icon" onClick={()=>deleteCard(item.id)}>
                                        <i className="far fa-trash-alt"></i>
                                    </div>
                                </tr>
                            })}
                            </tbody>
                        </table>
                        <div className="total">
                            <div className="total-content">
                                <div className="text">Subtotal</div>
                                <div className="total-price">${totalPrice()}</div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default ShoppingCard;