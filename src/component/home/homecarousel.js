import React from 'react';
import AliceCarousel from "react-alice-carousel";
import {Col, Row} from "antd";
import "../../style/home/homecarousel.scss";
function Homecarousel(props) {
    const responsive = {
        0: { items: 1 },
        568: { items: 1 },
        1024: { items: 1 },
    };
    const array=[
       <div className="item" key={1}  data-value={`1`}>
           <div className="bg-images">
               <Row>
                   <Col span={12}>

                   </Col>
                   <Col span={12}>
                       <div className="slider-content">
                           <div className="sub-title">
                               Mac parry
                           </div>
                           <div className="main-title">
                               Organic spirulina
                           </div>
                           <div className="msg">
                               Save Up to 40% off on your first Order
                           </div>
                           <div className="button-content">
                               <button className="btn">
                                   Shop now
                               </button>
                           </div>
                       </div>
                   </Col>
               </Row>
           </div>
       </div>,
        <div className="item" key={2}  data-value={`2`}>
            <div className="bg-images">
                <Row>
                    <Col span={12}>
                        <div className="slider-content">
                            <div className="sub-title">
                                big sale
                            </div>
                            <div className="main-title">
                                Fertile organics
                            </div>
                            <div className="msg">
                                Save Up to 20% off on your first Order
                            </div>
                            <div className="button-content">
                                <button className="btn">
                                    Shop now
                                </button>
                            </div>
                        </div>
                    </Col>
                    <Col span={12}>

                    </Col>
                </Row>
            </div>
        </div>
    ];
    return (
        <div className="home-carousel">
            <div className="container">
                <AliceCarousel
                    infinite={true}
                    mouseTracking
                    items={array}
                    responsive={responsive}
                    autoPlay={true}
                    autoPlayInterval={2000}
                />
            </div>
        </div>
    );
}

export default Homecarousel;