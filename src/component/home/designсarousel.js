import React from 'react';
import AliceCarousel from "react-alice-carousel";
import "../../style/home/design-carousel.scss";
import {Col, Row} from "antd";
function DesignCarousel(props) {
    const responsive = {
        0: { items: 1 },
        568: { items: 1 },
        1024: { items: 1 },
    };

    let array=[
        {
            "id": 1,
            "name": "John Duff",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/T3.png?v=1559733719",
            "msg": "Sed vitae placerat velit, non sempernib Mae cenas pharetra risus vitaesodales vulputate. Praesentaccumsan,minecorem IpsumsusSed vitae placerat velit, non sempernib Mae cenas pharetra risus vitaesodales vulputate. Praesentaccumsan,minecorem Ipsumsus"
        },
        {
            "id": 2,
            "name": "Luies Charls",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/T1.png?v=1559733713",
            "msg": "Sed vitae placerat velit, non sempernib Mae cenas pharetra risus vitaesodales vulputate. Praesentaccumsan,minecorem IpsumsusSed vitae placerat velit, non sempernib Mae cenas pharetra risus vitaesodales vulputate. Praesentaccumsan,minecorem Ipsumsus"
        },
        {
            "id": 3,
            "name": "Luies Charls",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/T2.png?v=1559733716",
            "msg": "Sed vitae placerat velit, non sempernib Mae cenas pharetra risus vitaesodales vulputate. Praesentaccumsan,minecorem IpsumsusSed vitae placerat velit, non sempernib Mae cenas pharetra risus vitaesodales vulputate. Praesentaccumsan,minecorem Ipsumsus"
        },
    ];
    let newArray=[];
    array.forEach((item, index)=>{
        newArray=newArray.concat(
            <div className="item" data-value={`${index}`} key={item.id}>
                <div className="msg">
                    <div className="msg-content">
                        {item.msg}
                    </div>
                </div>
                <div className="img-content">
                    <img src={item.url} alt={item.name}/>
                </div>
                <div className="name">{item.name}</div>
                <div className="job">Web Desginer</div>
            </div>
        )
    });
    return (
        <section className="design">
            <div className="seperator">
                <img
                    src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/title.png?v=1577861652"
                    alt="category image"
                />
            </div>
            <div className="design-text">
                Our Testimonial
            </div>
            <div className="design-carousel">
                <Row>
                    <Col span={14} offset={5}>
                        <AliceCarousel
                            infinite={true}
                            mouseTracking
                            items={newArray}
                            responsive={responsive}
                            controlsStrategy="alternate"
                        />
                    </Col>
                </Row>
            </div>
        </section>
    );
}

export default DesignCarousel;