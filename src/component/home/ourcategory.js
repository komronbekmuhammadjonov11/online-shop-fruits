import React from 'react';
import "../../style/home/ourcategory.scss";
import AliceCarousel from "react-alice-carousel";
function Ourcategory(props) {
    const responsive = {
        0: { items: 1 },
        568: { items: 2 },
        1024: { items: 4 },
    };
    let array=[
        {
            "id": 21,
            "name": "Apple",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C1.png?v=1559733666",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 22,
            "name": "Lemon",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C3.png?v=1559733672",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 23,
            "name": "Grapes",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C4.png?v=1559733675",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 24,
            "name": "Strawberry",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C5.png?v=1559893289",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 25,
            "name": "Orange",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C6.png?v=1559893291",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 26,
            "name": "Grapes",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C4.png?v=1559733675",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 27,
            "name": "Strawberry",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C5.png?v=1559893289",
            "msg": "There are many variations of available, but the"
        },
        {
            "id": 28,
            "name": "Orange",
            "url": "https://cdn.shopify.com/s/files/1/0086/0251/7559/files/C6.png?v=1559893291",
            "msg": "There are many variations of available, but the"
        },
    ];
    let newArray=[];
    array.forEach((item, index)=>{
        newArray=newArray.concat(
            <div className="item" key={item.id} data-value={`${index}`}>
                <div className="img-content">
                    <img src={item.url} alt={item.url}/>
                </div>
                <div className="title">{item.name}</div>
                <div className="msg">{item.msg}</div>
            </div>
        )
    });
    return (
        <section className="our-category">
            <div className="category-content">
                <div className="seperator">
                    <img
                        src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/title.png?v=1577861652"
                        alt="category image"
                    />
                </div>
                <div className="category-text">
                    Our Category
                </div>
                <div className="our-category-carousel">
                    <AliceCarousel
                        infinite={true}
                        mouseTracking
                        items={newArray}
                        responsive={responsive}
                        controlsStrategy="alternate"
                    />
                </div>
            </div>
        </section>
    );
}

export default Ourcategory;