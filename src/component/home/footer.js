import React from 'react';
import {Button, Col, Row} from "antd";
import "../../style/home/footer.scss";
function Footer(props) {
    return (
        <section className="footer">
            <div className="footer-top">
                <Row>
                    <Col span={12}>
                        <div className="footer-top-left">
                            <h2>Sign up & Get offer</h2>
                            <p>Sign up our newslatter And Get Latest News</p>
                            <form className="input-content">
                                <div className="input-group">
                                    <input type="email" required placeholder="Your email"/>
                                    <Button type="primary" htmlType="submit" >Subscribe</Button>
                                </div>
                            </form>
                        </div>
                    </Col>
                    <Col span={12}>
                        <div className="footer-top-right">
                            <h2>Follow Us</h2>
                            <p>Keep Up To Date with The Goings on here at Sport</p>
                            <div className="icon-group">
                                <ul className="icon-list">
                                    <li className="icon-list-item">
                                        <a href="#">
                                            <img src="/images/telegram.svg" alt="telegram"/>
                                        </a>
                                    </li>
                                    <li className="icon-list-item">
                                        <a href="#">
                                            <img src="/images/facebook.svg" alt="facebook"/>
                                        </a>
                                    </li>
                                    <li className="icon-list-item">
                                        <a href="#">
                                            <img src="/images/instagram.svg" alt="instagram"/>
                                        </a>
                                    </li>
                                    <li className="icon-list-item">
                                        <a href="#">
                                            <img src="/images/vkontak.svg" alt="vkontak"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
            <div className="footer-main">
                <Row>
                    <Col span={6}>
                        <div className="information">
                            <h2>STORE INFORMATION</h2>
                            <ul className="information-list">
                                <li className="information-list-item">
                                    <div className="item-left">
                                        <i className="fa fa-map-marker"></i>
                                    </div>
                                    <div className="item-right">
                                        <span>4005, Silver business point India</span>
                                    </div>
                                </li>
                                <li className="information-list-item">
                                    <div className="item-left">
                                        <i className="fa fa-phone"></i>
                                    </div>
                                    <div className="item-right">
                                        <a href="#">+91 123456789</a>
                                    </div>
                                </li>
                                <li className="information-list-item">
                                    <div className="item-left">
                                        <i className="fa fa-envelope"></i>
                                    </div>
                                    <div className="item-right">
                                        <a href="#">info@gmail.com</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className="account">
                            <h2>YOUR ACCOUNT</h2>
                            <ul>
                                <li>
                                    <a href="#">My Account</a>
                                </li>
                                <li>
                                    <a href="#">Personal Info</a>
                                </li>
                                <li>
                                    <a href="#">Orders</a>
                                </li>
                                <li>
                                    <a href="#">Credits Slips</a>
                                </li>
                                <li>
                                    <a href="#">Addresses</a>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className="company">
                            <h2> OUR COMPANY</h2>
                            <ul>
                                <li>
                                    <a href="#">Delivery</a>
                                </li>
                                <li>
                                    <a href="#">Legal Notice</a>
                                </li>
                                <li>
                                    <a href="#">Prices Drop</a>
                                </li>
                                <li>
                                    <a href="#">New Products</a>
                                </li>
                                <li>
                                    <a href="#">Best Sales</a>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className="gallery">
                            <h2>OUR GALLERY</h2>
                            <ul>
                                <li>
                                    <div className="hover-img">
                                        <img
                                            src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/2_de786d25-bc5e-4389-bc9a-8a6fdd4bbef5_500x500.png?v=1560337794"
                                            alt="gallery"
                                        />
                                        <div className="hover-div"></div>
                                    </div>
                                </li>
                                <li>
                                    <div className="hover-img">
                                        <img
                                            src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/3_37416915-be82-49cd-af80-01cac036f8a0_500x500.png?v=1560337798"
                                            alt="gallery"
                                        />
                                        <div className="hover-div"></div>
                                    </div>
                                </li>
                                <li>
                                    <div className="hover-img">
                                        <img
                                            src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/1_61c70a53-fafe-4fd5-a1b0-f77d516be1d9_500x500.png?v=1560337801"
                                            alt="gallery"
                                        />
                                        <div className="hover-div"></div>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div className="hover-img">
                                        <img
                                            src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/7_30c6c4b8-9d24-47bd-9e48-14039e5551ad_500x500.png?v=1560337805"
                                            alt="gallery"
                                        />
                                        <div className="hover-div"></div>
                                    </div>
                                </li>
                                <li>
                                    <div className="hover-img">
                                        <img
                                            src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/8_9177c71a-ceaf-4ce3-acab-c3af764755b0_500x500.png?v=1560337810"
                                            alt="gallery"
                                        />
                                        <div className="hover-div"></div>
                                    </div>
                                </li>
                                <li>
                                    <div className="hover-img">
                                        <img
                                            src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/10_500x500.png?v=1560337820"
                                            alt="gallery"
                                        />
                                        <div className="hover-div"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </Col>
                </Row>
            </div>
            <div className="footer-bottom">
                <Row>
                    <Col span={24}>
                        <div className="text">
                            © 2021, Organic Sectioned Shopify Theme Powered by Shopify
                        </div>
                    </Col>
                </Row>
            </div>
        </section>
    );
}

export default Footer;