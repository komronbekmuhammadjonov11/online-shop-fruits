import React from 'react';
import {Col, Row} from "antd";
import "../../style/home/fruitscategory.scss";
import fruitsCategory from "../../json/categoryfruits/tsconfig";
function Fruitscategory(props) {
    return (
        <section className="fruits-category">
            <div className="container">
                <Row>
                    {fruitsCategory.fruits.map((item)=>{
                        return <Col span={6}>
                            <div className="card" style={{backgroundImage:`url(${item.url})`}}>
                                <div className="card-content">
                                    <div className="title">
                                        {item.name}
                                    </div>
                                    <div className="description">
                                        {item.description}
                                    </div>
                                    <button>
                                        Shop now
                                    </button>
                                </div>
                            </div>
                        </Col>
                    })}
                </Row>
            </div>
        </section>
    );
}

export default Fruitscategory;