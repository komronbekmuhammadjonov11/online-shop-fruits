import React, {useContext, useState} from 'react';
import {Select, Button, Row, Col} from 'antd';
import {Link, useHistory} from "react-router-dom";
import {DataContext} from "../../App";

const { Option } = Select;
function Navbar(props) {
    const history=useHistory();
    const {card}=useContext(DataContext);
    const [product, setProduct]=useState("");

    const handleInputPress=(e)=>{
        if (e.key==="Enter"){
            history.push(`/search/${product}`);
        }
    };
    function totalPrice() {
        let sum=0;
        let amount=0;
        if (card){
            card.forEach((item)=>{
                sum+=item.price*item.amount;
                amount+=item.amount;
            });
        }
        return {sum , amount};
    }
    function handleChange(value) {
        console.log(value); // { value: "lucy", key: "lucy", label: "Lucy (101)" }
    }
    return (
        <section className="navbar">
            <div className="navbar-top">
                <div className="container">
                    <p>Welcome To Our Fruit’s Online Store</p>
                    <ul className="top-list">
                        <li className="top-list-item">
                            <Button type="primary">Wish List</Button>
                        </li>
                        <li className="top-list-item">
                            <Select defaultValue="Account" style={{width: 120}} onChange={handleChange}>
                                <Option value="jack">Account</Option>
                            </Select>
                        </li>
                        <li className="top-list-item">
                            <Select
                                labelInValue
                                defaultValue={{value: 'USD'}}
                                style={{width: 120}}
                                onChange={handleChange}
                            >
                                <Option value="jack">USD $</Option>
                                <Option value="lucy">CAD $</Option>
                                <Option value="lucy">EUR Э</Option>
                                <Option value="lucy">USD $</Option>
                            </Select>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="navbar-main">
               <div className="container">
                   <Row>
                       <Col span={8}>
                           <div className="input-content">
                               <input
                                   type="text"
                                   placeholder="Search"
                                   name="product"
                                   onChange={(e)=>setProduct(e.target.value)}
                                   onKeyPress={handleInputPress}
                               />
                               <button>
                                    <i className="fa fa-search"></i>
                               </button>
                           </div>
                       </Col>
                       <Col span={8}>
                           <Link to="/" className="brand">
                               <img
                                   src="//cdn.shopify.com/s/files/1/0086/0251/7559/files/Logo_large_large_ac20412f-d60c-4b97-905e-085295e70357_large.png?v=1578047151"
                                   alt="logotip"/>
                           </Link>
                       </Col>
                       <Col span={8}>
                           <ul className="menu-bar-list">
                               <li className="menu-bar-list-item">
                                   <div className="email-content">
                                       <div className="email-icon">

                                       </div>
                                       <div className="vertical-line"></div>
                                       <div className="email-text">
                                            <span className="title">
                                                Email Us
                                            </span>
                                           <div className="description">
                                                info@gmail.com
                                           </div>
                                       </div>
                                   </div>
                               </li>
                               <li className="menu-bar-list-item">
                                   <Link to="/shopping" className="basket-content">
                                       <div className="basket-icon">

                                       </div>
                                       <div className="vertical-line"></div>
                                       <div className="basket-text">
                                            <span className="title">
                                                Card Item
                                            </span>
                                           <div className="description">
                                               ({totalPrice().amount}) $ {totalPrice().sum}
                                           </div>
                                       </div>
                                   </Link>
                               </li>
                           </ul>
                       </Col>
                   </Row>
               </div>
            </div>
            <div className="navbar-bottom">
                <div className="container">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a href="#" className="nav-link">Organic</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" className="nav-link">Accessories</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" className="nav-link">Category</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" className="nav-link">Includes Pages</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" className="nav-link">Faq</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" className="nav-link">About Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    );
}

export default Navbar;