import React from 'react';
import Navbar from "./navbar";
import "../../style/home/navbar.scss";
import Fruitscategory from "./fruitscategory";
import Productcarousel from "./productcarousel";
import Ourcategory from "./ourcategory";
import Services from "./services";
import DesignCarousel from "./designсarousel";
import Company from "./company";
import Footer from "./footer";
import Homecarousel from "./homecarousel";
function Home(props) {
    return (
        <div className="home-page">
            <Navbar/>
            <Homecarousel/>
            <Fruitscategory/>
            <Productcarousel/>
            <Ourcategory/>
            <Services/>
            <DesignCarousel/>
            <Company/>
            <Footer/>
        </div>
    );
}

export default Home;