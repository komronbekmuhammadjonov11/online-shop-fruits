import React from 'react';
import {Col, Row} from "antd";
import "../../style/home/services.scss";
function Services(props) {
    return (
        <section className="services">
            <Row>
                <Col span={6}>
                    <div className="card-content">
                        <div className="img-content">
                            <div className="icon-img"></div>
                        </div>
                        <div className="title">Free Shipping</div>
                        <div className="msg">
                            Lorem ipsum dolor sit amet.
                        </div>
                    </div>
                </Col>
                <Col span={6}>
                    <div className="card-content">
                        <div className="img-content">
                            <div className="icon-img"></div>
                        </div>
                        <div className="title">Money Back</div>
                        <div className="msg">
                            Lorem ipsum dolor sit amet.
                        </div>
                    </div>
                </Col>
                <Col span={6}>
                    <div className="card-content">
                        <div className="img-content">
                            <div className="icon-img"></div>
                        </div>
                        <div className="title">Online Support</div>
                        <div className="msg">
                            Lorem ipsum dolor sit amet.
                        </div>
                    </div>
                </Col>
                <Col span={6}>
                    <div className="card-content">
                        <div className="img-content">
                            <div className="icon-img"></div>
                        </div>
                        <div className="title">Special Gift</div>
                        <div className="msg">
                            Lorem ipsum dolor sit amet.
                        </div>
                    </div>
                </Col>
            </Row>
        </section>
    );
}

export default Services;