import React, {useContext, useState} from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import "../../style/home/productcarousel.scss";
import fruitsJson from "../../json/fruits/tsconfig";
import {Col, Modal, Rate, Row} from "antd";
import {Link} from "react-router-dom";
import {DataContext} from "../../App";

function Productcarousel(props) {
    const {data, card , setCard}=useContext(DataContext);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const initialState={
        url:"",
        name:"",
        price:"",
        id:"",
        amount:"",
        rate:0
    };
    const [product, setProduct] = useState(initialState);
    const showModal = (id) => {
        setIsModalVisible(true);
        let newProduct;
        newProduct =data.find((item) => item.id === id);
        console.log(newProduct);
        setProduct(newProduct);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };
    const handleCancel = () => {
        setIsModalVisible(false);
        setProduct(initialState);
    };
    const addCard=()=>{
        let newArray=[];
        if (localStorage.getItem("card")){
            if (card.find(item=>item.id===product.id)){
                newArray=card.map(item2=>{
                    if (item2.id===product.id){
                        return {
                            ...item2,
                            amount:product.amount+item2.amount
                        }
                    }else return item2;
                });
                setCard(newArray);
                localStorage.setItem("card", JSON.stringify(newArray));
            }else {
                newArray=JSON.parse(localStorage.getItem("card")).concat(product);
                localStorage.setItem("card", JSON.stringify(newArray));
                setCard(newArray);
            }
        }else {
            localStorage.setItem("card", JSON.stringify([product]));
            setCard([product]);
        }
        handleCancel();
        window.scroll(0,0);
    };
    const addCardTwo=(id)=>{
        let newObject=data.find(item=>item.id===id);
        let newArray=[];
        if (localStorage.getItem("card")){
            if (card.find(item=>item.id===id)){
                newArray=card.map(item2=>{
                    if (item2.id===id){
                        return {
                            ...item2,
                            amount:item2.amount+1
                        }
                    }else return item2;
                });
                setCard(newArray);
                localStorage.setItem("card", JSON.stringify(newArray));
            }else {
                newArray=JSON.parse(localStorage.getItem("card")).concat(newObject);
                localStorage.setItem("card", JSON.stringify(newArray));
                setCard(newArray);
            }
        }else {
            localStorage.setItem("card", JSON.stringify([newObject]));
            setCard([newObject]);
        }
        window.scroll(0,0);

    };
    const decrement=(e)=>{
        if (product.amount>1){
            setProduct(()=>{
                return {
                    ...product,
                    amount:product.amount-1
                }
            })
        }
    };
    const increment=()=>{
        setProduct(()=>{
            return {
                ...product,
                amount:product.amount+1
            }
        })
    };
    const responsive = {
        0: {items: 1},
        568: {items: 2},
        1024: {items: 5},
    };
    let array = [];
    data.forEach((item, index) => {
        if ( array.length < 7) {
            array = array.concat(<div className="item" data-value={`${index}`} key={item.id}>
                <div className="card">
                    <div className="card-header">
                        <img src={item.url} alt={item.url}/>
                    </div>
                    <div className="card-body">
                        <div className="starts">
                            <Rate disabled defaultValue={item.rate}/>
                        </div>
                        <div className="name">{item.name}</div>
                        <div className="price">
                            ${item.price}
                        </div>
                        <div className="icon-bar">
                            <ul className="icon-list">
                                <li className="icon-list-item">
                                    <div className="icon" onClick={() => showModal(item.id)}></div>
                                </li>
                                <li className="icon-list-item">
                                    <div className="icon" onClick={()=>addCardTwo(item.id)}></div>
                                </li>
                                <li className="icon-list-item">
                                    <div className="icon"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>);
        }
    });
    // console.log(product);
    return (
        <section className="product-carousel">
            <div className="container">
                <div className="separator">
                    <img
                        src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/title.png?v=1577861652"
                        alt="images"
                    />
                </div>
                <div className="title">Our Products</div>
                <AliceCarousel
                    infinite={true}
                    mouseTracking
                    items={array}
                    responsive={responsive}
                    controlsStrategy="alternate"
                />
                <Modal title="Product" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className="modal-content">
                        <Row>
                            <Col span={12}>
                                <div className="img-content">
                                    <img src={product.url} alt={product.name}/>
                                </div>
                            </Col>
                            <Col span={12}>
                                <div className="text-content">
                                    <div className="name">{product.name}</div>
                                    <div className="price">{product.price}</div>
                                    <div className="starts">
                                        <Rate disabled value={product.rate}/>
                                    </div>
                                    <div className="msg">
                                        Faded short sleeves t-shirt with high neckline. Soft and stretchy material for a
                                        comfortable fit. Accessorize with a straw hat and you're ready for summer!
                                    </div>
                                    <Link to="/" className="link">
                                        Read More
                                    </Link>
                                    <div className="btn-content">
                                        <span className="btn-group">
                                            <button className="decrement" onClick={decrement}>
                                                -
                                            </button>
                                            <button className="count">
                                                {product.amount}
                                            </button>
                                            <button className="increment" onClick={increment}>
                                                +
                                            </button>
                                        </span>
                                    </div>
                                    <button className="btn-add" onClick={addCard}>
                                        Add to card
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Modal>
            </div>
        </section>
    );
}

export default Productcarousel;