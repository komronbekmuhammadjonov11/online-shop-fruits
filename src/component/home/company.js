import React from 'react';
import AliceCarousel from "react-alice-carousel";
import "../../style/home/company.scss";
function Company(props) {
    const responsive = {
        0: { items: 1 },
        568: { items: 4 },
        1024: { items: 6 },
    };
    let array=[
        <div className="item" data-value={`1`} key={1}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-4.png?v=1559892548" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-4.png?v=1559892548"/>
        </div>,
        <div className="item" data-value={`2`} key={2}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-5.png?v=1559892552" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-5.png?v=1559892552"/>
        </div>,
        <div className="item" data-value={`3`} key={3}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-6.png?v=1559892554" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-6.png?v=1559892554"/>
        </div>,
        <div className="item" data-value={`4`} key={4}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-1.png?v=1559892542" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-1.png?v=1559892542"/>
        </div>,
        <div className="item" data-value={`5`} key={5}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-2.png?v=1559892545" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-2.png?v=1559892545"/>
        </div>,
        <div className="item" data-value={`6`} key={6}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-3.png?v=1559892547" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-3.png?v=1559892547"/>
        </div>,
        <div className="item" data-value={`7`} key={7}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-4.png?v=1559892548" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-4.png?v=1559892548"/>
        </div>,
        <div className="item" data-value={`8`} key={8}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-5.png?v=1559892552" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-5.png?v=1559892552"/>
        </div>,
        <div className="item" data-value={`9`} key={9}>
            <img src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-6.png?v=1559892554" alt="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Brand-Logo-6.png?v=1559892554"/>
        </div>,
    ];
    return (
        <section className="company-carousel">
            <AliceCarousel
                infinite={true}
                mouseTracking
                items={array}
                responsive={responsive}
                controlsStrategy="alternate"
            />
        </section>
    );
}

export default Company;