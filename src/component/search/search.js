import React, {useContext, useEffect, useState} from 'react';
import "../../style/search/search.scss";
import Navbar from "../home/navbar";
import Footer from "../home/footer";
import {Col, Modal, Rate, Row} from "antd";
import {DataContext} from "../../App";
import {Link, useParams} from "react-router-dom";

function Search(props) {
    const {data, card , setCard}=useContext(DataContext);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const {name}=useParams();
    const [searchDate, setSearchDate]=useState([]);
    useEffect(()=>{
        let array=[];
        array=data.filter(item=>item.name.toUpperCase().includes(name.toUpperCase()));
        if (array){
            setSearchDate(array)
        }
        console.log(array);

    }, [name]);
    const initialState={
        url:"",
        name:"",
        price:"",
        id:"",
        amount:"",
        rate:0
    };
    const [product, setProduct] = useState(initialState);
    const showModal = (id) => {
        setIsModalVisible(true);
        let newProduct;
        newProduct =data.find((item) => item.id === id);
        console.log(newProduct);
        setProduct(newProduct);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };
    const handleCancel = () => {
        setIsModalVisible(false);
        setProduct(initialState);
    };
    const addCard=()=>{
        let newArray=[];
        if (localStorage.getItem("card")){
            if (card.find(item=>item.id===product.id)){
                newArray=card.map(item2=>{
                    if (item2.id===product.id){
                        return {
                            ...item2,
                            amount:product.amount+item2.amount
                        }
                    }else return item2;
                });
                setCard(newArray);
                localStorage.setItem("card", JSON.stringify(newArray));
            }else {
                newArray=JSON.parse(localStorage.getItem("card")).concat(product);
                localStorage.setItem("card", JSON.stringify(newArray));
                setCard(newArray);
            }
        }else {
            localStorage.setItem("card", JSON.stringify([product]));
            setCard([product]);
        }
        handleCancel();
        window.scroll(0,0);
    };
    const addCardTwo=(id)=>{
        let newObject=data.find(item=>item.id===id);
        let newArray=[];
        if (localStorage.getItem("card")){
            if (card.find(item=>item.id===id)){
                newArray=card.map(item2=>{
                    if (item2.id===id){
                        return {
                            ...item2,
                            amount:item2.amount+1
                        }
                    }else return item2;
                });
                setCard(newArray);
                localStorage.setItem("card", JSON.stringify(newArray));
            }else {
                newArray=JSON.parse(localStorage.getItem("card")).concat(newObject);
                localStorage.setItem("card", JSON.stringify(newArray));
                setCard(newArray);
            }
        }else {
            localStorage.setItem("card", JSON.stringify([newObject]));
            setCard([newObject]);
        }
        window.scroll(0,0);

    };
    const decrement=(e)=>{
        if (product.amount>1){
            setProduct(()=>{
                return {
                    ...product,
                    amount:product.amount-1
                }
            })
        }
    };
    const increment=()=>{
        setProduct(()=>{
            return {
                ...product,
                amount:product.amount+1
            }
        })
    };
    return (
        <>
            <Navbar/>
            <section className="search-page">
                <div className="container">
                    <Row>
                        <Col span={7}>
                            <div className="img-baner">
                                <img
                                    src="https://cdn.shopify.com/s/files/1/0086/0251/7559/files/Left-banner_6b9b6278-c0b2-4c75-a6ac-ab39bae0615d.png?v=1560170863"
                                    alt="baner"
                                />
                            </div>
                        </Col>
                        <Col span={17}>
                            <div className="search-content">
                                <div className="title">Search Our Catalogue</div>
                                {searchDate.length>0?
                                    (
                                        <ul className="list">
                                            {searchDate.map(item=>{
                                                return <li className='list-item'>
                                                    <Row>
                                                        <Col span={8}>
                                                            <div className="img-content">
                                                                <img
                                                                    src={item.url}
                                                                    alt={item.name}/>

                                                            </div>
                                                        </Col>
                                                        <Col span={16}>
                                                            <div className="rate">
                                                                <Rate disabled value={item.rate}/>
                                                            </div>
                                                            <div className="name">{item.name}</div>
                                                            <div className="price">${item.price}</div>
                                                            <div className="msg">
                                                                Faded short sleeves t-shirt with high neckline. Soft and stretchy
                                                                material for a comfortable fit. Accessorize with a straw hat and
                                                                you're ready for summer! Sample Unordered List Comodous in tempor...
                                                            </div>
                                                            <div className="icon-bar">
                                                                <ul className="icon-list">
                                                                    <li className="icon-list-item">
                                                                        <div className="icon"  onClick={() => showModal(item.id)} ></div>
                                                                    </li>
                                                                    <li className="icon-list-item">
                                                                        <div className="icon" onClick={()=>addCardTwo(item.id)} ></div>
                                                                    </li>
                                                                    <li className="icon-list-item">
                                                                        <div className="icon"></div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </li>
                                            })}
                                        </ul>
                                    )
                                    :
                                    (
                                        <div className="no-date">
                                            Hozircha malumotlar mavjud emas.
                                        </div>
                                    )
                                }
                            </div>
                        </Col>
                    </Row>
                </div>
                <Modal title="Product" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className="modal-content">
                        <Row>
                            <Col span={12}>
                                <div className="img-content">
                                    <img src={product.url} alt={product.name}/>
                                </div>
                            </Col>
                            <Col span={12}>
                                <div className="text-content">
                                    <div className="name">{product.name}</div>
                                    <div className="price">{product.price}</div>
                                    <div className="starts">
                                        <Rate disabled value={product.rate}/>
                                    </div>
                                    <div className="msg">
                                        Faded short sleeves t-shirt with high neckline. Soft and stretchy material for a
                                        comfortable fit. Accessorize with a straw hat and you're ready for summer!
                                    </div>
                                    <Link to="/" className="link">
                                        Read More
                                    </Link>
                                    <div className="btn-content">
                                        <span className="btn-group">
                                            <button className="decrement" onClick={decrement}>
                                                -
                                            </button>
                                            <button className="count">
                                                {product.amount}
                                            </button>
                                            <button className="increment" onClick={increment}>
                                                +
                                            </button>
                                        </span>
                                    </div>
                                    <button className="btn-add" onClick={addCard}>
                                        Add to card
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Modal>
            </section>
            <Footer/>
        </>
    );
}

export default Search;