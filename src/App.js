import React, {createContext, useState} from "react";
import Home from "./component/home";
import "./App.css";
import "./style/mixins/colors/colors.scss";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Basket from "./component/basket";
import fruitsJson from "./json/fruits/tsconfig";
import Search from "./component/search/search";

export const DataContext=createContext(null);


function App() {
    let data=[];
    const local=()=>{
        if (localStorage.getItem("data")){
            data=JSON.parse(localStorage.getItem("data"));
            return data;
        }else {
            data=fruitsJson.fruits;
            return data
        }
    };
    const [card, setCard]=useState(localStorage.getItem? JSON.parse(localStorage.getItem("card")): []);
    return (
        <div className="App">
            <DataContext.Provider value={{data:local(), card, setCard}}>
                <BrowserRouter>
                    <Switch>
                        <Route  path="/shopping" component={Basket}/>
                        <Route path="/search/:name" component={Search}/>
                        <Route  path="/"  component={Home}/>
                    </Switch>
                </BrowserRouter>
            </DataContext.Provider>
        </div>
    );
}

export default App;
